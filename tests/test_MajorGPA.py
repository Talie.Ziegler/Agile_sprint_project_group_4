import pytest
from gpa import Course,GPA
def test_Major_GPA():
    stu1 = GPA()
    stu1.addCourse(Course("A","COMP", 4, "Fall 2023", "P"))
    stu1.addCourse(Course("C","WRIT", 2, "Spring 2022", "C"))
    stu1.addCourse(Course("B+", "MATH", 4,"Winter 2024", "I"))
    assert stu1.major_GPA_calculator("MATH") == 3.33

test_Major_GPA()