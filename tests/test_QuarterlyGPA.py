import pytest
from gpa import Course,GPA

def test_QuarterGPA_with_single_course():
    gpa = GPA()
    gpa.courses.append(Course("A", "Math", 4, "Q1", "Completed"))
    result = gpa.quarterly_gpa()
    assert result == pytest.approx(4.0, 0.01)

def test_QuarterGPA_with_multiple_courses():
    gpa = GPA()
    gpa.courses.append(Course("A", "Math", 4, "Q1", "Completed"))
    gpa.courses.append(Course("B+", "Science", 3, "Q1", "Completed"))
    gpa.courses.append(Course("C-", "English", 2, "Q1", "Completed"))
    result = gpa.quarterly_gpa()
    assert result == pytest.approx(3.2566666666666, 0.01)

def test_QuarterGPA_with_invalid_grade():
    gpa = GPA()
    gpa.courses.append(Course("A", "Math", 4, "Q1", "Completed"))
    gpa.courses.append(Course("X", "Science", 3, "Q1", "Completed"))  
    with pytest.raises(ValueError) as e:
        gpa.quarterly_gpa()
    assert str(e.value) == "Bad grade"

def test_QuarterGPA_with_zero_total_credits():
    gpa = GPA()
    gpa.courses.append(Course("A", "Math", 0, "Q1", "Completed"))
    gpa.courses.append(Course("B", "Science", 0, "Q1", "Completed"))
    gpa.courses.append(Course("C", "English", 0, "Q1", "Completed"))
    with pytest.raises(ZeroDivisionError):
        gpa.quarterly_gpa()