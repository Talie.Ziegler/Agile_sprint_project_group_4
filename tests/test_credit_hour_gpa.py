import pytest 
from gpa import Course,GPA

def test_credit_hour_gpa():
    student1 = GPA()
    student1.addCourse(Course("A","COMP", 4, "Fall 2023", "P"))
    student1.addCourse(Course("C+","WRIT", 2, "Spring 2022", "C"))
    student1.addCourse(Course("B-", "MATH", 4,"Winter 2024", "W"))
    assert student1.calculate_credit_hour_gpa() == 3.13
test_credit_hour_gpa()

