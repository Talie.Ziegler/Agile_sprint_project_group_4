from gpa import GPA,Course
import pytest


def test_GPA_Predict():
    stu1 = GPA()
    stu1.addCourse(Course("A","COMP", 4, "Fall 2023", "P"))
    stu1.addCourse(Course("B","WRIT", 2, "Spring 2022", "C"))
    stu1.addCourse(Course("C", "MATH", 4,"Winter 2024", "W"))
    assert stu1.GPA_Predict() == 3.5

