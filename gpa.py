
# Class that holds course information for a student
#  and calculates their GPA
class Course:
    def __init__(self, grade: str, department: str, credit_hour:int, quarter:str, status:str):
        self.grade = grade
        self.department = department
        self.credit_hour = credit_hour
        self.quarter = quarter
        self.status = status


class GPA():
    def __init__(self):
        self.courses = []

    def addCourse(self, grade):
        self.courses.append(grade)

    def calculateGPA(self):
        total = 0.0
        for course in self.courses:
            grade = course.grade
            if grade == "A+":
                total += 4.33
            elif grade == "A":
                total += 4.0
            elif grade == "A-":
                total += 3.66
            elif grade == "B+":
                total += 3.33
            elif grade == "B":
                total += 3.0
            elif grade == "B-":
                total += 2.66
            elif grade == "C+":
                total += 2.33
            elif grade == "C":
                total += 2.0
            elif grade == "C-":
                total += 1.66
            elif grade == "D+":
                total += 1.33
            elif grade == "D":
                total += 1.0
            elif grade == "D-":
                total += 0.66
            elif grade == "F":
                total += 0.0
            else:
                raise ValueError("Bad grade")
            
        return total/len(self.courses)

    def major_GPA_calculator(self, major):
        total = 0.0
        count = 0
        for course in self.courses:
            if course.department == major:
                count += 1
                grade = course.grade
                if grade == "A+":
                    total += 4.33
                elif grade == "A":
                    total += 4.0
                elif grade == "A-":
                    total += 3.66
                elif grade == "B+":
                    total += 3.33
                elif grade == "B":
                    total += 3.0
                elif grade == "B-":
                    total += 2.66
                elif grade == "C+":
                    total += 2.33
                elif grade == "C":
                    total += 2.0
                elif grade == "C-":
                    total += 1.66
                elif grade == "D+":
                    total += 1.33
                elif grade == "D":
                    total += 1.0
                elif grade == "D-":
                    total += 0.66
                elif grade == "F":
                    total += 0.0
                else:
                    raise ValueError("Bad grade")
        return total/count

    def GPA_Predict(self):
        total = 0.0
        #added count
        count = 0
        for course in self.courses:
            if course.status == "C" or course.status == "P" or course.status == "Complete" or course.status == "Predict":
                #increase count for each course being counted towards predicted GPA
                count += 1
                grade = course.grade
                if grade == "A+":
                    total += 4.33
                elif grade == "A":
                    total += 4.0
                elif grade == "A-":
                    total += 3.66
                elif grade == "B+":
                    total += 3.33
                elif grade == "B":
                    total += 3.0
                elif grade == "B-":
                    total += 2.66
                elif grade == "C+":
                    total += 2.33
                elif grade == "C":
                    total += 2.0
                elif grade == "C-":
                    total += 1.66
                elif grade == "D+":
                    total += 1.33
                elif grade == "D":
                    total += 1.0
                elif grade == "D-":
                    total += 0.66
                elif grade == "F":
                    total += 0.0
                else:
                    raise ValueError("Bad grade")  
        #counts the number of predicted credits over the number of predicted credit hours    
        return total/count
    
    def quarterly_gpa(self):
        total = 0.0
        total_credits = 0
        for course in self.courses:
            grade = course.grade
            if grade == "A+":
                total += 4.33 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "A":
                total += 4.0 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "A-":
                total += 3.66 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "B+":
                total += 3.33 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "B":
                total += 3.0 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "B-":
                total += 2.66 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "C+":
                total += 2.33 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "C":
                total += 2.0 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "C-":
                total += 1.66 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "D+":
                total += 1.33 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "D":
                total += 1.0 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "D-":
                total += 0.66 * course.credit_hour
                total_credits += course.credit_hour
            elif grade == "F":
                total += 0.0 * course.credit_hour
                total_credits += course.credit_hour
            else:
                raise ValueError("Bad grade")
            
        return total/total_credits
    
    def calculate_credit_hour_gpa(self):
        total_grade_points = 0.0
        total_credit_hours = 0

        for course in self.courses:
            grade = course.grade
            credit_hour = course.credit_hour
            if grade == "A+":
                grade_points = 4.33
            elif grade == "A":
                grade_points = 4.0
            elif grade == "A-":
                grade_points = 3.66
            elif grade == "B+":
                grade_points = 3.33
            elif grade == "B":
                grade_points = 3.0
            elif grade == "B-":
                grade_points = 2.66
            elif grade == "C+":
                grade_points = 2.33
            elif grade == "C":
                grade_points = 2.0
            elif grade == "C-":
                grade_points = 1.66
            elif grade == "D+":
                grade_points = 1.33
            elif grade == "D":
                grade_points = 1.0
            elif grade == "D-":
                grade_points = 0.66
            elif grade == "F":
                grade_points = 0.0
            else:
                raise ValueError("Bad grade")
            total_grade_points += grade_points * credit_hour
            total_credit_hours += credit_hour
        if total_credit_hours == 0:
            return 0.0
        return total_grade_points/total_credit_hours


# Temporary Testing code
stu1 = GPA()
stu1.addCourse(Course("A","COMP", 4, "Fall 2023", "P"))
stu1.addCourse(Course("C","WRIT", 2, "Spring 2022", "C"))
stu1.addCourse(Course("B+", "MATH", 4,"Winter 2024", "I"))

stu1GPA = stu1.calculateGPA()
stu1GPA = stu1.GPA_Predict()
stu1GPA = stu1.quarterly_gpa()
print(stu1GPA)
