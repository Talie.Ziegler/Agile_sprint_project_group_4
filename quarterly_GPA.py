"""
Calculating quarterly GPA
Name: Kaylin Francisco
Class: COMP 1203
Group: 4
Group Mates: Adam, Christopher, Talie
Date: 5/18/23
"""
"""
Made own copy of the grading system and edited it to fit the quarter system
and for user input
"""
class Course:
    def __init__(self, grade: str, department: str, credit_hour:int, quarter:str):
        self.grade = grade
        self.department = department
        self.credit_hour = credit_hour
        self.quarter = quarter
        
class GPA():
    """
    grades have same amount of credit,
    just initializing it differently
    """
    def __init__(self):
        self.points = {
            'A+':4.33,
            'A':4.0,
            'A-':3.66,
            'B+':3.33,
            'B':3.0,
            'B-':2.66,
            'C+':2.33,
            'C':2.0,
            'C-':1.66,
            'D+':1.33,
            'D':1.0,
            'D-':0.66,
            'F':0.0,
        }

    def calculate_quarter_gpa(self, grades):
        """
        made seperate definition to calculate quarterly gpa
        """
        total_credits = 0
        total_grade_points = 0
        # finding class credits
        for course, grade, credits in grades:
            if grade in self.points:
                credit_points = self.points[grade]*credits
                total_grade_points += credit_points
                total_credits += credits
        # classes have no credits
        if total_credits == 0:
            return 0
        # calculating gpa
        gpa= total_grade_points/total_credits
        return gpa
    
    def user_data(self):
        """
        taking in user data and storing it
        """
        courses = input("Enter the course name: ")
        grade = input("Enter grade recieved: ")
        credits = float(input("Enter number of credits: "))
        return courses, grade, credits

    def quarterly_gpa(self):
        """
        gathering data based on number of classes user inputs,
        and adds them to the course_info
        """
        num_courses = int(input("Enter total number of courses: "))
        grades = []
        
        for i in range(num_courses):
            course_info = self.user_data()
            grades.append(course_info)
        # calculating gpa and returning to user
        quarterly_gpa = self.calculate_quarter_gpa(grades)
        print(f"Quarterly GPA: {quarterly_gpa}")

# test code
# final_grades = [('Math', 'A', 4), ('English', 'B+', 3), ('COMP', 'A-', 3)]
# expected_gpa: 3.6

calculator = GPA()
print(calculator.quarterly_gpa())